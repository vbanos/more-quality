# MoRe Quality - Metadata evaluation tool prototype
This application won the first prize at the [LoCloud Hackathon](http://hackathon.locloud.eu/)
which was conducted on the 11th of February 2015 at the premises of the Google Cultural Institute in Paris, France.

The author is [Vangelis Banos](http://vbanos.gr/) who is working with
[Future Library](http://www.futurelibrary.gr/) from Greece.

## Concept
A lot of people are using the MoRe aggregator to gather metadata and publish them to Europeana.
The problem is that the ingested metadata are not validated correctly and are usually problematic.

Metadata may pass the standard Europeana XML structure tests but they may include problematic metadata values.
For instance, a dc:date value could be like this: <dc:date>approximately 18th century</dc:date> or an author
name may be something like this: <dc:creator>Mike</dc:creator>.

The aim of the MoRe quality tool is to implement a validation system which could be able to catch
these errors and produce useful reports to the collection administrators.

## Features
MoRe Quality communicates with the LoCloud MoRe repository using a specific user API Key,
retrieves the ingested metadata and performs evaluations to identify common
errors such as:
* Invalid date formats (ISO 8601 standard)
* Invalid hyperlinks
* Invalid language codes (ISO 639 standard)
* Invalid author names

## Technology
MoRe Quality is implemented using python 2.7. The following python modules are used:
* Virtual environments
* Flask
* Python Requests
* BeautifulSoup4
* pycountry
* iso8601
A list of all required python modules is included in the source code in the requirements.txt file.

##  Future Work
* New validation rules could be implemented using standard python functions.
* The system can become configurable, enabling the user to select the rules she/he would like to apply to each metadata element.
* The system could send feedback to the MoRe aggregator with technical metadata regarding the identified issues.
*Dear MoRe developers, please make the API to enable this in the future.*.
* Performance
* UI
