# -*- coding: utf-8 -*-
## @author Vangelis Banos
""" Communication with MORE Harvester """
import json
import requests
import time

from settings import MORE_BASE_URL, MORE_HTTP_HEADERS

def list_packages():
    response = requests.get(MORE_BASE_URL + "/packages", headers=MORE_HTTP_HEADERS)
    return response.json()

def list_metadata_sources():

    url = MORE_BASE_URL + "/metadataSources"
    response = requests.get(MORE_BASE_URL + "/metadataSources", headers=MORE_HTTP_HEADERS)
    return response.json()

def list_metadata_source_info(identifier):
    response = requests.get(MORE_BASE_URL + "/metadataSources/%d" % identifier, headers=MORE_HTTP_HEADERS)
    return response.json()

def get_packages(identifier, schema):
    response = requests.get(MORE_BASE_URL + "/packages/%d/datastreams/%s" % (identifier, schema), headers=MORE_HTTP_HEADERS)
    return response.json()
