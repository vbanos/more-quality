# -*- coding: utf-8 -*-
## @author Vangelis Banos
##
"""Check if author name is according to bibliographic metadata standards

must check
https://bibtexparser.readthedocs.org/en/latest/tutorial.html
bibliograph.parsing

"""
import re
from bs4 import BeautifulSoup

from lib.utils import ValidationResultWrapper, ValidationResult, Status, \
    bs4_complex_query, rdf_get_preflabel

@ValidationResultWrapper
def check(value, params=None):
    """Check if author name is according to bibliographic metadata standards
    https://owl.english.purdue.edu/owl/resource/560/06/
    https://pypi.python.org/pypi/bibliograph.parsing/1.0.0
    Panagiotatos Mitropolitis Athinas Vasileios
    V. Banos
    Banos, V.
    Banos V.,
    repository spec 12
    """
    if not value:
        return False
    parts = re.split('[?, ]', value)
    parts = filter(None, parts)
    if len(parts) > 4:
        raise Exception("Author name too long.")
    elif len(parts) == 1:
        raise Exception("Author name too short.")

    word_limit = 2
    big_words = 0
    for word in parts:
        if "(" in word or ")" in word or "[" in word or "]" in word:
            raise Exception("Author name cannot contain parentheses or brackets")
        if word.isdigit():
            raise Exception("Author name cannot contain numbers.")
        if len(word) > word_limit:
            big_words = big_words + 1
    if big_words == 0:
        raise Exception("Author name is too short.")
    return True

def edm_check(element_xml, params=None):
    """
    <dc:creator rdf:resource="http://mouseio.avmap.gr/vocab/vocab/?tema=737"/>
    ...
    <rdf:Agent rdf:about="http://mouseio.avmap.gr/vocab/vocab/?tema=737">
    <skos:prefLabel xml:lang="el">Άγνωστος δημιουργός</skos:prefLabel>
    </rdf:Agent>
    """
    soup = BeautifulSoup(element_xml, "xml")
    for element in soup.children:
        if element.get("resource") and params.get("record_xml"):
            resource = element.get("resource")
            author_str = rdf_get_preflabel(resource, params.get("record_xml"))
            if not author_str:
                return ValidationResult(
                    status=Status.Critical,
                    exception="no skos:prefLabel in element " + resource
                )
            return check(author_str, params)

        elif element.string:
            return check(element.string, params)

    return ValidationResult(
        status=Status.Critical,
        exception="Could not find proper dc:creator"
    )
