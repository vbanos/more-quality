# -*- coding: utf-8 -*-
## @author Vangelis Banos
##
"""date related validation functions"""
from bs4 import BeautifulSoup
from datetime import date
from iso8601 import parse_date

def year(value, params=None):
    """Check if string is a year"""
    if not value:
        return False
    value = str(value)
    if value.startswith("-"):
        value = value[1:]
    return len(value) <= 4 and len(value) >= 1 and value.isdigit() and int(value) >= 0 and int(value) <= (date.today().year + 10)

def iso8601(value, params=None):
    """Check if string is a date according to iso8601 standard"""
    if not value:
        return False
    is_year = year(value)
    if is_year:
        return False
    if value.startswith("-"):
        value = value[1:]
    try:
        parse_date(value)
        return True
    except Exception, exc:
        return False
