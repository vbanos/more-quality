# -*- coding: utf-8 -*-
## @author Vangelis Banos
##
"""language related validation functions"""
import pycountry

def iso639(value):
    """Check if string is a language according to iso639 standard"""
    try:
        pycountry.languages.get(terminology=value)
        return True
    except:
        return False
