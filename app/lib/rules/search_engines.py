# -*- coding: utf-8 -*-
## @author Vangelis Banos
##
"""Check if a digital library is accessible by search engines"""
from requests import head
from lib.utils import ValidationResultWrapper, get_xml_record_urls

from etc.settings import HTTP_CONNECTION_TIMEOUT

@ValidationResultWrapper
def check(record_xml, params=None):
    """Must load record, find URL and try to access it to see if it works
    If true, then search engines can also access it"""
    urls = get_xml_record_urls(str(record_xml))
    if not urls:
        raise Exception("No URLs could be found in the record.")
    for url in urls:
        response = head(url, timeout=HTTP_CONNECTION_TIMEOUT)
        if response.status_code in xrange(200, 399):
            return True
    raise Exception("No URL in the record leads to a live html page.")
