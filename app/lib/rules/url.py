# -*- coding: utf-8 -*-
## @author Vangelis Banos
##
"""Module with URL validation methods. Call like this in rulesets.json

    "//europeana:isShownAt": {
        "url.exists",
        "url.syntax"
    }
"""
from bs4 import BeautifulSoup
import requests
from urlparse import urlparse

def permanent_identifier(url):
    """CHECK if url is using DOI"""
    if not url:
        return False
    parsed = urlparse(url)
    return parsed.netloc == "dx.doi.org" or parsed.netloc == "hdl.handle.net"

def exists(url):
    """Send an HTTP HEAD to see if a url exists"""
    if not url:
        return False
    try:
        response = requests.head(url)
        return response.status_code in xrange(200, 399)
    except:
        return False

def edm_exists(element_xml):
    """Validate this:
    <rdf:isShownBy rdf:resource="http://hdl.handle.net/11412/l_1051"/>
    and also this:
    <rdf:isShownBy>http://hdl.handle.net/11412/l_1051</rdf:isShownBy>
    """
    soup = BeautifulSoup(element_xml, "xml")
    for element in soup.children:
        if element.get("resource"):
            return exists(element.get("resource"))
        elif element.string:
            return exists(element.string)

def syntax(url, params=None):
    """Check URL syntax using python urlparse module"""
    if not url:
        return False
    parsed = urlparse(url)
    return parsed.netloc != ''
