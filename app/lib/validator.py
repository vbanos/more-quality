# -*- coding: utf-8 -*-
## @author Vangelis Banos
""" Communication with MORE Harvester """
from bs4 import BeautifulSoup

import requests
import time

import rules.date
import rules.language
import rules.url

from settings import MORE_BASE_URL, MORE_HTTP_HEADERS, EDM_RULES

def statistics(results):
    statistics = {}
    statistics['total'] = len(results)
    statistics['evaluations'] = 0
    
    for item in results:
        statistics['evaluations'] += len(item['results'])

    return statistics

def process(data):
    output =[]
    for item in data:
        result = requests.get(item['uri'])
        item['xml'] = result.text
        item['results'] = edm_validate(item['xml'])
        output.append(item)
    return output


def edm_validate(xml):
    output = []
    soup = BeautifulSoup(xml)

    for rule in EDM_RULES:
        elements = soup.find_all(rule['element'])
        out = {
            'element': rule['element'],
            'found': elements,
            'rule': rule['rule'],
            'results': ''
        }
        if elements and len(elements) > 0:
            tmp = ""
            if "iso639" in out['rule']:
                for elem in elements:
                    if not rules.language.iso639(elem.get_text()):
                        tmp = tmp + "There is a problem with the format of the language " + str(elem)
            elif "permanent" in out['rule']:
                for elem in elements:
                    if not rules.url.permanent_identifier(elem.attrs['rdf:resource']):
                        tmp = tmp + "The url is not permanent " + str(elem)
            elif "exists" in out['rule']:
                for elem in elements:
                    if not rules.url.exists(elem.attrs['rdf:resource']):
                        tmp = tmp + "The url does not exist " + str(elem)
            if tmp == "":
                tmp = "OK"
            out['results'] = tmp
        output.append(out)
    return output
