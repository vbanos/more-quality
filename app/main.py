# -*- coding: utf-8 -*-
# @author Vangelis Banos
#
"""Main morequality web interface"""

from flask import Flask, render_template

import lib.more
import lib.validator

import settings

App = Flask(__name__)

App.config['CSRF_ENABLED'] = settings.CSRF_ENABLED
App.config['SECRET_KEY'] = settings.SECRET_KEY
App.config.settings = settings

@App.route("/", methods=['GET'])
def home():
    return render_template('home.html')

@App.route("/packages")
def list_packages():
    packages = lib.more.list_packages()
    print packages

    return render_template('packages.html', packages=packages)

@App.route("/packages/check/<int:identifier>/<schema>")
def check_package(identifier, schema):
    # TODO fix
    schema = 'EDM'
    packages = lib.more.get_packages(identifier, schema)
    results = lib.validator.process(packages)
    statistics = lib.validator.statistics(results)
    return render_template('package-check.html', identifier=identifier, results=results, statistics=statistics, EDM_RULES=settings.EDM_RULES)

@App.route("/metadata-sources")
def list_metadata_sources():
    sources = lib.more.list_metadata_sources()
    return render_template('metadata-sources.html', sources=sources)

@App.route("/metadata-sources/info/<int:identifier>")
def view_metadata_source_info(identifier):
    source = lib.more.list_metadata_source_info(identifier)
    return render_template('metadata-source-info.html', source=source)

@App.route("/metadata-sources/check/<int:identifier>/<schema>")
def check(identifier, schema):
    packages = lib.more.get_packages(identifier, schema)
    return render_template('metadata-source-check.html', packages=packages)
