# -*- coding: utf-8 -*-
# @author Vangelis Banos
"""
Main Configuration File
"""
import logging

APP_NAME = 'MORe Quality'
APP_DESCRIPTION = 'MORe Metadata Quality Evaluation Tool'

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,  # this fixes the problem
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'INFO',
            'class':'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': True
        }
    }
}

DEBUG = True

ROOT_DIR = "/var/www/morequality/"

# LOGGING
LOG_DIR = "/var/www/morequality/logs/"
LOG_ROTATION_SIZE = 1000000 # 1 MB
# position of UPDATE in the hierarchy of log levels
UPDATE_LEVEL = 5

# string containing the format of the logs in the same form as python's logging.Formatter
LOG_FORMAT = '%(levelname)s::%(asctime)s::%(name)s -  - %(message)s'
ERROR_LOG_NAME = "error.log"
ACCESS_LOG_NAME = "access.log"
UPDATE_LOG_NAME = "update.log"
# END LOGGING

PORT = 8080
HOST = "vbanos.gr"
BASE_URL = "http://vbanos.gr:8080/"

USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; rv:14.0) Gecko/20100101 Firefox/14.0.1"

HTTP_REQUESTS_PER_SECOND = 1
HTTP_CONNECTION_TIMEOUT = 10

# RQ Queue name
RQUEUE = 'webgraph-it-dev'
RQUEUE_TIMEOUT = 900
RQUEUE_WAIT_INTERVAL = 2

TMP_DIR = "/tmp/"

CSRF_ENABLED = True
SECRET_KEY = '12371982371982371'

# Paging
PER_PAGE = 10

MORE_BASE_URL = "http://more.locloud.eu:8080/MoRe_API/api/v1"

MORE_HTTP_HEADERS = {
    'Content-type': 'application/json',
    'accept': 'application/json',
    'X-API-Key': 'JFeDWPV9NhG7gTzqTrk2'
}

EDM_RULES = [
    {'element': "dc:date", 'rule': 'date.iso8601'},
    {'element': "dc:language", 'rule': 'language.iso639'},
    {'element': "edm:isshownat", 'rule': 'url.exists'},
    {'element': "edm:isshownat", 'rule': 'url.permanent'}
]
